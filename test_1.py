import os

def _sum(var1, var2):
    return var1 + var2


if '__main__' ==  __name__:
    v1 = os.environ.get('value1', 1)
    v2 = os.environ.get('value2', 1)
    print(_sum(v1, v2))